package com.project.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author raitis
 * 
 * Controller for initial pages - home, information, "My Jobs" for courier profile
 */ 
@Controller
public class HomeController {

	/**
	 * @return
	 * 
	 * First initial page controller
	 */
	@RequestMapping("/")
	 public String showHome() {
	
		return "home";
		 
	 }	

}
