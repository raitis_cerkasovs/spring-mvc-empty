package com.project.controllers;

import com.project.dao.User;
import com.project.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

/**
 * @author raitis
 *
 * Functions related to user account
 */
@Controller
public class LoginController {

	/**
	 * Initialize service
	 */
	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}


	/**
	 * @return
	 *
	 * Function to create user
	 */
	@RequestMapping(value = "/createuser")
	public String createAccount(Model model) {

		User user = new User();
		model.addAttribute("user", user);

		return "createuser";
	}


	/**
	 * @param model - not used
	 * @param user - user object
	 * @param result - validator's error list
	 * @return
	 *
	 * Function to save updated user
	 */
	@RequestMapping(value = "/saveuser")
	public String saveUser(Model model, @Valid User user, BindingResult result) {

		if (result.hasErrors()) {
			return "createuser";
		}
		usersService.saveUser(user, "ROLE1");

		return "home";
	}



	
	/**
	 * @param model - pass "user" parameter to view
	 * @param un - username
	 * @return
	 * 
	 * Function to open form for user data editing
	 */
	@RequestMapping(value = "/editaccount")
	public String editAccount(Model model, @RequestParam("un") String un) {
		
		User user = usersService.getUser(un);
		model.addAttribute("user", user);

		return "editaccount";
	}
	
	
	/**
	 * @param model - not used
	 * @param user - user object
	 * @param result - validator's error list
	 * @return
	 * 
	 * Function to save updated user
	 */
	@RequestMapping(value = "/updateaccount")
	public String updateAccount(Model model, @Valid User user, BindingResult result) {
		
		if (result.hasErrors()) {
			return "editaccount";
		} 			
		usersService.saveOrUpdateUser(user);
		
		return "home";
	}


	/**
	 * @return
	 * 
	 * Login function (integrated from SpringMVC)
	 */
	@RequestMapping("/login")
	public String showLogin() {
		return "home";
	}


	/**
	 * @return
	 * Logout function (integrated from SpringMVC)
	 */
	@RequestMapping("/logout")
	public String showLogout() {
		return "home";
	}


	/**
	 * @return
	 * 
	 * Function to return "accessdenied view"
	 */
	@RequestMapping("/accessdenied")
	public String showAccessDenied() {
		return "accessdenied";
	}



}