package com.project.controllers;

import com.project.dao.*;

import com.project.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


/**
 * @author raitis
 *
 * All REST services
 */

@Controller
public class RestController {
	
	

	@Autowired
	private UsersService usersService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
		
	
//	/**
//	 * @param username - courier's username
//	 * @param lat - latitude from android positioning
//	 * @param lon longitude from android positioning
//	 * @return
//	 *
//	 * RESTful service, gathering courier's usename, latitude and longitude, updating them in database,
//	 * and returning JSON data with courier's jobs list
//	 */
//	@RequestMapping(value="/getjobsjson", method= RequestMethod.GET, produces="application/json")
//	@ResponseBody
//	public Map<String, Object> getjobsJson(@RequestParam("uid") String username,
//			                               @RequestParam("lat") String lat,
//			                               @RequestParam("lon") String lon){
//
//		Courier courier = bookingsService.getCourier(username);
//
//		courier.setLat(Float.valueOf(lat));
//		courier.setLon(Float.valueOf(lon));
//
//		courier.setIsworking(true);
//
//		usersService.saveOrUpdateCourier(courier);
//
//        List<Job> jobs= bookingsService.getCourierJobsJson(username);
//
//        Map<String, Object> data = new HashMap<String, Object>();
//        data.put("jobs", jobs);
//
//		return data;
//	}
	


}
