package tests;

import static org.junit.Assert.*;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.dao.Authorities;
import com.project.dao.User;
import com.project.dao.UsersDAO;

/**
 * @author raitis
 * 
 * Used to test main functionality for each DAO function in observed class
 */
@ActiveProfiles("development")
@ContextConfiguration(locations = {
		"classpath:config/dao-context.xml",
		"classpath:config/security-context.xml",
		"classpath:config/test/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDAOtests {
		
		@Autowired
		private UsersDAO usersDao;
		
		@Autowired
		private DataSource dataSource;
		
		@Autowired
		private PasswordEncoder passwordEncoder;

		Authorities authority;
		User user1;
		User user2;

		@Before
		public void init() {
			JdbcTemplate jdbc = new JdbcTemplate(dataSource);
			
			//clean the tables
			jdbc.execute("delete from feedbacks");
			jdbc.execute("delete from jobs");
			jdbc.execute("delete from sendto");
			jdbc.execute("delete from sendfrom");
			jdbc.execute("delete from clients");
			jdbc.execute("delete from couriers");
			jdbc.execute("delete from authorities");
            jdbc.execute("delete from users");	

			//register authority
			authority = new Authorities();
			authority.setUsername("testusername");
			authority.setAuthority("testautority");
			
			//register user1
		    user1 = new User();
			user1.setUsername("testusername");
			user1.setPassword("testpassword");
			user1.setEmail("test@email.com");
			user1.setName("testame");
			user1.setSurname("testsurname");
			user1.setAuthorities(authority);
			user1.setPhone("123");			
			user1.setAddress1("testaddress");			
			user1.setAddress2("testaddress2");
			user1.setPostcode("testpostcode");
			user1.setEnabled(false);	
		
			//register user2
			user2 = new User();			
			user2.setUsername("testusername2");
			user2.setPassword("testpassword2");
			user2.setEmail("test2@email.com");
			user2.setName("testame2");
			user2.setSurname("testsurname2");
			user2.setAuthorities(authority);
			user2.setPhone("123");			
			user2.setAddress1("testaddress2");			
			user2.setAddress2("testaddress2");
			user2.setPostcode("testpostcode2");
			user2.setEnabled(true);

		}
		
		/**
		 * Test for save(), getUsers() and getAllUsers() functions
		 */
		@Test
		public void testSaveUser() {

			//add one user
            usersDao.save(user1);	      
 	    	List<User> usersList = usersDao.getAllUsers();
			
		    assertEquals("One user should have been created and retrieved", 1, usersList.size());	
			
		    //add second user
			usersDao.save(user2);
     		usersList = usersDao.getAllUsers();

    		assertEquals("Number of users should be 2.", 2, usersList.size());
 	    
    		//user, where courier property is "working" is true and role is "testauthority"
     	    List<User> workingUsers = usersDao.getUsers("testautority");

     		assertEquals("List must contain one user.", 1, workingUsers.size());   		
		}
		
		
	    
		/**
		 * Test for saveOrUpdate() and getUser() functions
		 */
		@Test
		public void testSaveOrUpdateUser() {
			
			//add one user
            usersDao.save(user1);	

			//get same user back
            User user3 = usersDao.getUser("testusername");	
   		
	        assertEquals("User3 username must be the same as user1", "testusername", user3.getUsername());	
			
	        //change field
            user3.setName("MendedName");
            usersDao.saveOrUpdate(user3);
            
            //get same user back
            User user4 = new User();
            user4 = usersDao.getUser("testusername");	
            
	        assertEquals("User4 must have changed name", "MendedName", user4.getName());	

		}
		
}