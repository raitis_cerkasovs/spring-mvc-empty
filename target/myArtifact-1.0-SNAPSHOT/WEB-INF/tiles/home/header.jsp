<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">



    <%--identify menu--%>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar">asd</span>
        <span class="icon-bar">sf</span>
        <span class="icon-bar">sfef</span>
      </button>
      <a class="navbar-brand" href="#">Project name</a>
    </div>



    <%--left navbar--%>
    <div id="navbar" class="navbar-collapse collapse">



      <%--left menu--%>
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#contact">Contact</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li class="dropdown-header">Nav header</li>
            <li><a href="#">Separated link</a></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>



      <%--unautorized login--%>
      <sec:authorize access="!isAuthenticated()">

        <ul class="nav navbar-nav navbar-right">
          <li><a href="<c:url value='/createuser' />">New</a></li>
        </ul>

        <c:if test="${param.error != null}">
          <ul class="nav navbar-nav navbar-right">
            <li><a style="color: red;">Wrong Credentials</a></li>
          </ul>
        </c:if>

        <form class="navbar-form navbar-right" name='f' action='${pageContext.request.contextPath}/j_spring_security_check' method='POST'>

        <div class="form-group">
          <input type="text" placeholder="Email" class="form-control" name='j_username' value=''>
        </div>

        <div class="form-group">
          <input type="password" placeholder="Password" class="form-control" name='j_password'/>
        </div>

        <button type="submit" class="btn btn-success">Sign in</button>

          <div class="form-group">
            <input type='checkbox' name='_spring_security_remember_me' checked="checked">
          </div>

      </form>
      </sec:authorize>



      <%--authorized login--%>
      <sec:authorize access="isAuthenticated()">
        <ul class="nav navbar-nav navbar-right">
            <li><a style="color: red;"><sec:authentication property="principal.username" /></a></li>
          <sec:authorize access="hasRole('ROLE_COURIER')">
            <li><a style="color: red;">ROLE1</a></li>
          </sec:authorize>
          <sec:authorize access="hasRole('ROLE_CUSTOMER')">
            <li><a style="color: red;">ROLE2</a></li>
          </sec:authorize>
          <li><a href="<c:url value='/editaccount?un=${pageContext.request.userPrincipal.name}'/>">Edit Account</a></li>
          <li><a href="<c:url value="/j_spring_security_logout" />">Logout</a></li>
        </ul>
      </sec:authorize>




    </div><!--navbar-collapse -->



  </div>
</nav>