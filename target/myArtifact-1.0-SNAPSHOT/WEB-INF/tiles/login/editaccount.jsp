<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Edit Profile:</h1>
 
<sf:form method="POST" action="${pageContext.request.contextPath}/updateaccount" commandName="user" role="form">

<sf:hidden path="password"/>
<sf:hidden path="enabled"/>

    <div class="form-group">
        <label for="username">Username</label>
        <label class="error" for="username"><sf:errors path="username" /></label>
        <sf:input path="username" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="name">Name</label>
        <label class="error" for="name"><sf:errors path="name" /></label>
        <sf:input path="name" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="surname">Surname</label>
        <label class="error" for="surname"><sf:errors path="surname" /></label>
        <sf:input path="surname" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <label class="error" for="email"><sf:errors path="email" /></label>
        <sf:input path="email" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="phone">Phone</label>
        <label class="error" for="phone"><sf:errors path="phone" /></label>
        <sf:input path="phone" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="address1">Address</label>
        <label class="error" for="address1"><sf:errors path="address1" /></label>
        <sf:input path="address1" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="address2">Address</label>
        <label class="error" for="address2"><sf:errors path="address2" /></label>
        <sf:input path="address2" type="text" class="form-control" />
    </div>

    <div class="form-group">
        <label for="postcode">Postcode</label>
        <label class="error" for="postcode"><sf:errors path="postcode" /></label>
        <sf:input path="postcode" type="text" class="form-control" />
    </div>

    <button type="submit" class="btn btn-default">Submit</button>

</sf:form>